<%@include file="head.jsp" %>

<h2 class="form-signin-heading">Your Account</h2>

<form method="post" action="Auth">

	<input type="hidden" name="formtype" value="update" />
	
	<input type="hidden" name="id" value="${ user.id }" />
	
	<div class="form-group">
   		<label for="inputLogin">Login</label>
		<input type="text" name="login" class="form-control" id="inputLogin" placeholder="Login" value="${ user.login }" />
	</div>
	
	<div class="form-group">
		<label for="inputPassword">Password</label>
		<input type="text" name="password" class="form-control" id="inputPassword" placeholder="Password" value="${ user.password }" />
	</div>
	
	<div class="form-group">
   		<label for="inputEmail">Adresse eMail</label>
		<input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" value="${ user.email }" />
	</div>
	
	<div class="form-group">
   		<label for="inputFirstName">Prenom</label>
		<input type="text" name="firstname" class="form-control" id="inputFirstName" placeholder="Pr�nom" value="${ user.firstname }" />
	</div>
	<div class="form-group">
   		<label for="inputLastName">Nom</label>
		<input type="text" name="lastname" class="form-control" id="inputLastName" placeholder="Nom" value="${ user.lastname }" />
	</div>
	<div class="form-group">
   		<label for="inputBirthday">Date d'anniversaire</label>
		<input type="date" name="birthday" class="form-control" id="inputBirthday" placeholder="Date d'anniversaire" value="${ user.birthday }" />
	</div>
	
<button type="submit" class="btn btn-default">Mettre � jour</button>
</form>

<a href="" style="color:red">D�connection</a>

<%@include file="foot.jsp" %>