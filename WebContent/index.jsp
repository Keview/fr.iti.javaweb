<%@include file="WEB-INF/head.jsp" %>
<form class="form-signin" method="POST" action="Auth">
	
	<input type="hidden" name="formtype" value="auth" />
	
	<h2 class="form-signin-heading">Please sign in</h2>
	
	<label for="inputLogin" class="sr-only">Email address</label>
	
	<input type="text" id="inputLogin" class="form-control" placeholder="Login" name="login" required autofocus>
        
	<label for="inputPassword" class="sr-only">Password</label>
	
	<input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
	
	<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
</form>
<%@include file="WEB-INF/foot.jsp" %>