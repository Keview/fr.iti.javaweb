package fr.iti.s5.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.iti.s5.jweb.beans.User;
import fr.iti.s5.jweb.modeles.Modele;


public class Auth extends HttpServlet {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
		this.getServletContext().getRequestDispatcher( "/index.jsp" ).forward( request, response );
	}
	
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
		try {
	        Modele modele;
			modele = new Modele();
			
			String formtype = request.getParameter( "formtype" );
			if(formtype.equals("auth")) {
				String login = request.getParameter( "login" );
		        String password = request.getParameter( "password" );
		        int userID = modele.connectUser(login, password);
		        if(userID!=0) {
		        	User user = modele.getUserByID(userID);
					request.setAttribute( "user", user );
		        	this.getServletContext().getRequestDispatcher( "/WEB-INF/update.jsp" ).forward( request, response );
		        } else {
		        	this.getServletContext().getRequestDispatcher( "/WEB-INF/auth_fail.jsp" ).forward( request, response );
		        }
			} else {
				int userID = Integer.parseInt(request.getParameter( "id" ));
				String login = request.getParameter( "login" );
				String password = request.getParameter( "password" );
				String email = request.getParameter( "email" );
				String firstname = request.getParameter( "firstname" );
				String lastname = request.getParameter( "lastname" );
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date birthday = format.parse(request.getParameter( "birthday" ));
				User user = new User(userID, lastname, firstname, email, birthday, login, password);
				modele.updateUser(user);
				user = modele.getUserByID(userID);
				request.setAttribute( "user", user );
	        	this.getServletContext().getRequestDispatcher( "/WEB-INF/update.jsp" ).forward( request, response );
			}
		} catch (Exception e) {}
	}
}
