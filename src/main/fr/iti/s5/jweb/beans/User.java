package fr.iti.s5.jweb.beans;

import java.util.Date;

public class User {
	public User(Integer id, String lastname, String firstname, String email, Date birthday, String login,
			String password) {
		super();
		this.id = id;
		this.lastname = lastname;
		this.firstname = firstname;
		this.email = email;
		this.birthday = birthday;
		this.login = login;
		this.password = password;
	}

	private Integer id;
	private String lastname;
	private String firstname;
	private String email;
	private Date birthday;
	
	private String login;
	private String password;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer pId) {
		id = pId;
	}
	
	public String getLastname() {
		return lastname;
	}
	
	public void setLastname(String pLastname) {
		lastname = pLastname;
	}
	
	public String getFirstname() {
		return firstname;
	}
	
	public void setFirstname(String pFirstname) {
		firstname = pFirstname;
	}
	
	public Date getBirthday() {
		return birthday;
	}
	
	public void setBirthday(Date pBirthday) {
		birthday = pBirthday;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String pEmail) {
		this.email = pEmail;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String pLogin) {
		login = pLogin;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String pPassword) {
		password = pPassword;
	}
}
