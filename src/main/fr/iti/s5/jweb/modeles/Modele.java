package fr.iti.s5.jweb.modeles;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import fr.iti.s5.jweb.beans.User;

public class Modele {
	
	Connection connection;
	
	public Modele() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/jweb", "root", "");
		setConnection(connection);
	}
	
	public Modele(Connection pConnection) {
		setConnection(pConnection);
	}
	
	/**
	 * @return the connection
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * @param connection the connection to set
	 */
	public void setConnection(Connection pConnection) {
		connection = pConnection;
	}

	/**
	 * Cette méthode test la connection d'un user à la base
	 * @param pUser
	 * @return
	 */
	public int connectUser(String login, String password) {
		String selectSQL = "SELECT id FROM user WHERE login = '"+login+"' AND password='" + password +"'";
		int r = 0;
		Statement statement;
		ResultSet rs;
		try {
			statement = connection.createStatement();
			rs = statement.executeQuery(selectSQL);
			if (rs.next()) {
				r = rs.getInt("id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return r;
	}
	
	/**
	 * Cette méthode recupére un user dans la base 
	 * @return
	 */
	public User getUserByID(int pID) {
		String selectSQL = "SELECT * FROM user WHERE id = "+pID;
		User user = null;
		
		Statement statement;
		ResultSet resultat;
		try {
			statement = connection.createStatement();
			resultat = statement.executeQuery(selectSQL);
			if(resultat.next()) {
				user=new User(	
						pID,
						resultat.getString( "nom" ), 
						resultat.getString( "prenom" ), 
						resultat.getString( "email" ), 
						resultat.getDate( "date_naissance" ),
						resultat.getString( "login" ), 
						resultat.getString( "password" )
				);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
	/**
	 * Cette méthode met à jour un user
	 * @param pUser
	 * @return
	 */
	public void updateUser(User pUser) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
		String selectSQL = "UPDATE user SET nom = '"+pUser.getLastname()+
				"', prenom = '"+pUser.getFirstname()+
				"', email = '"+pUser.getEmail()+
				"', date_naissance = '"+sdf.format(pUser.getBirthday())+
				"', login = '"+pUser.getLogin()+
				"', password = '"+pUser.getPassword()+
				"' WHERE id = "+pUser.getId();
		try {
			Statement statement;
			statement = connection.createStatement();
			statement.executeUpdate(selectSQL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
