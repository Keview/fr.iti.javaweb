package fr.iti.s5.jweb.modeles;

import fr.iti.s5.jweb.beans.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.Properties;

import com.xeiam.yank.PropertiesUtils;
import com.xeiam.yank.Yank;

public class ModeleTest {
	
	//public static final String SQL_USER_TABLE = "CREATE TABLE user (id INT NOT NULL IDENTITY, login VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, birthday DATE NOT NULL)";
	//public static final String SQL_USER_TABLE = "CREATE TABLE user (id INT NOT NULL IDENTITY, login VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, date_naissance DATETIME NOT NULL)";
	//public static final String SQL_USER_INSERT = "INSERT INTO user VALUES (1, 'blackpower', 'mandela', 'OBAMA', 'Barack', 'president@washington.us', '1961-08-04 00:00:00');";
	public static final String SQL_USER_TABLE = "CREATE TABLE user (id INT NOT NULL IDENTITY, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_naissance DATETIME NOT NULL, email VARCHAR(255) NOT NULL, login VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL)";
	public static final String SQL_USER_INSERT = "INSERT INTO user VALUES (1, 'OBAMA', 'Barack', '1961-08-04 00:00:00', 'president@washington.us', 'blackpower', 'mandela');";	
    @Before
    public void setUp() {
    	Properties dbProps = PropertiesUtils.getPropertiesFromClasspath("HSQL_DB.properties");

        Yank.setupDataSource(dbProps);

        Yank.execute(SQL_USER_TABLE, null);
        Yank.execute(SQL_USER_INSERT, null);
    }
    
    @After
    public void tearDownDB() {
    	Yank.releaseDataSource();
    }
	
	@Test
	public void testConnectUser() throws SQLException {
		
		Connection con = Yank.getDataSource().getConnection();
		Modele modele = new Modele(con);
		int userID;
		
		userID = modele.connectUser("blackpower","mandela");
		Assert.assertTrue(userID==1);
		
		userID = modele.connectUser("blackpower","123");
		Assert.assertFalse(userID==1);
	}
	
	@Test
	public void testGetUserByID() throws SQLException {
		
		Connection con = Yank.getDataSource().getConnection();
		Modele modele = new Modele(con);
		
		User user;
		
		user = modele.getUserByID(1);
		Assert.assertNotNull(user);

		Assert.assertEquals(user.getFirstname(), "Barack");
		Assert.assertEquals(user.getLastname(), "OBAMA");
		Assert.assertEquals(user.getLogin(), "blackpower");
		Assert.assertEquals(user.getPassword(), "mandela");
		Assert.assertEquals(user.getEmail(), "president@washington.us");
		Assert.assertEquals(user.getBirthday().toString(), "1961-08-04");
		Assert.assertTrue(user.getId()==1);
		
		user = modele.getUserByID(94);
		Assert.assertNull(user);
	}
	
	@Test
	public void testUpdateUser() throws SQLException {

		Connection con = Yank.getDataSource().getConnection();
		Modele modele = new Modele(con);
		
		User user;
		
		user = modele.getUserByID(1);
		Assert.assertNotNull(user);
		
		
		String newFirstname = "Jeff";
		String newLastname = "COOKIE";
		Date newBirthday = new Date();
		String newLogin = "Jicook";
		String newPassword = "toctoc";
		String newEmail = "jicook@toujours.paris";
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Assert.assertNotEquals(newFirstname, user.getFirstname());
		Assert.assertNotEquals(newLastname, user.getLastname());
		Assert.assertNotEquals(sdf.format(newBirthday), sdf.format(user.getBirthday()));
		Assert.assertNotEquals(newLogin, user.getLogin());
		Assert.assertNotEquals(newPassword, user.getPassword());
		Assert.assertNotEquals(newEmail, user.getEmail());
		
		user.setFirstname(newFirstname);
		user.setLastname(newLastname);
		user.setBirthday(newBirthday);
		user.setLogin(newLogin);
		user.setPassword(newPassword);
		user.setEmail(newEmail);
		
		modele.updateUser(user);
		user = modele.getUserByID(1);
		
		Assert.assertEquals(newFirstname, user.getFirstname());
		Assert.assertEquals(newLastname, user.getLastname());
		Assert.assertEquals(sdf.format(newBirthday), sdf.format(user.getBirthday()));
		Assert.assertEquals(newLogin, user.getLogin());
		Assert.assertEquals(newPassword, user.getPassword());
		Assert.assertEquals(newEmail, user.getEmail());
	}
}
